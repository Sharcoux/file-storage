const exec = require('child_process').exec;
const path = require('path');
const fs = require('fs').promises;

//Create the storage folder
const STORAGE_PATH = process.env.STORAGE_PATH;
const BACKUP_PATH = process.env.BACKUP_PATH;
const PROTECTED_PATHS = [path.relative('.', STORAGE_PATH), path.relative('.', BACKUP_PATH)]

require('fs').mkdirSync(path.normalize(BACKUP_PATH), { recursive: true });
require('fs').mkdirSync(path.normalize(STORAGE_PATH), { recursive: true });

/** Return the size of a directory in octet */
async function _getDirectorySize(path) {
  return new Promise((resolve, reject) => {
    if(!require('fs').existsSync(path)) return 0
    exec('du -s '+path, function(error, stdout, stderr){
      if (error !== null){
        console.error('exec error: du -s', path, '\n', error);
        reject(error);
      } else {
        console.error(stderr);
        resolve(parseInt(stdout.split(/\s/)[0], 10));
      }
    });
  });
}

/** Return the backup location for the provided file */
function _getBackup(file) {
  return file.replace(path.normalize(STORAGE_PATH), path.normalize(BACKUP_PATH));
}

/** Try and remove a file if it exists */
async function _removeIfExists(file) {
  return fs.unlink(file).catch(err => err.code === 'ENOENT' ? Promise.resolve('done') : Promise.reject(err));
}

/** Get the location to the user storage folder */
function _getUserStorage(userId) {
  return path.normalize(path.join(STORAGE_PATH, userId+''));
}

/** Get the location of a file in the user storage */
function _getFilePath(userId, filename) {
  return path.normalize(path.join(STORAGE_PATH, userId+'', filename));
}

/** Recursively create the parents folder and then create the file denoted by the file path */
async function _createFileAndParents(file, content) {
  const folder = path.dirname(file);
  return fs.mkdir(folder, {recursive: true})
    .then(() => fs.writeFile(file, content));
}

/** Delete the file and recursively delete any parent folder that became empty */
async function _deleteFileAndEmptyParents(filename) {
  const folder = path.dirname(filename);
  async function clean(folder) {
    if (PROTECTED_PATHS.includes(folder))
      return true
    return fs.rmdir(folder)
      .then(() => clean(path.dirname(folder)))                      //Recursive cleaning of empty directorues
      .catch(err => err.code==='ENOTEMPTY' || Promise.reject(err)); //Ignore not empty directory errors
  }
  return _removeIfExists(filename).then(() => clean(folder));
}

/** Delete the target file if exists, rename the file, and delete any parent folder of the previous location that became empty */
async function _renameFileAndHandleParents(oldFile, newFile) {
  const folder = path.dirname(newFile);
  return fs.mkdir(folder, {recursive: true})          //Create the destination folder
    .then(() => _removeIfExists(newFile))              //Remove the previous file if exists
    .then(() => fs.rename(oldFile, newFile))          //Rename the file
    .then(() => _deleteFileAndEmptyParents(oldFile));  //Delete any empty folder from the previous location
}

/** Restore the backup of provided filename if exists */
async function restoreBackup(userId, filename) {
  const file = _getFilePath(userId, filename);
  const backup = _getBackup(file);
  return _renameFileAndHandleParents(backup, file);
}

/** Create a backup for the specified file */
async function backupFile(userId, filename) {
  const file = _getFilePath(userId, filename);
  const backup = _getBackup(file);
  return _renameFileAndHandleParents(file, backup);
}

/** Clear the backup of the provided file */
async function removeBackup(userId, filename) {
  const backup = _getBackup(_getFilePath(userId, filename));
  return _deleteFileAndEmptyParents(backup);
}

/** Create a file belonging to the user */
async function writeFile(userId, filename, data) {
  const file = _getFilePath(userId, filename);
  return _createFileAndParents(file, data);
}

/** Return the content of target file */
async function readFile(userId, filename) {
  const file = _getFilePath(userId, filename);
  return fs.readFile(file);
}

/** Delete the target file and remove parent folder if they became empty */
async function removeFile(userId, filename) {
  const file = _getFilePath(userId, filename);
  return _deleteFileAndEmptyParents(file);
}

/** Rename a file. Destroy or create folder as needed. */
async function renameFile(userId, oldName, newName) {
  const oldFile = _getFilePath(userId, oldName);
  const newFile = _getFilePath(userId, newName);
  return _renameFileAndHandleParents(oldFile, newFile);
}

/** Return the used space of user storage */
async function getUsedSpace(userId) {
  return _getDirectorySize(_getUserStorage(userId));
}

/** Prepare the storage for user userId */
async function createUserStorage(userId) {
  return fs.mkdir(_getUserStorage(userId));
}

/** Delete all the files and folder of target user */
async function destroyUserStorage(userId) {
  return fs.rmdir(_getUserStorage(userId), {recursive: true});
}

async function fileExist(userId, filename) {
  const file = _getFilePath(userId, filename);
  return fs.access(file);
}

module.exports = {
  createUserStorage,
  destroyUserStorage,
  getUsedSpace,
  backupFile,
  restoreBackup,
  removeBackup,
  renameFile,
  writeFile,
  readFile,
  removeFile,
  fileExist,
};