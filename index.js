/* eslint-disable require-atomic-updates */
const { createServer, is, none, all, plugins: { loginPlugin } } = require('simple-ql');
const express = require('express');
const { createUserStorage, destroyUserStorage, restoreBackup, removeBackup, fileExist, backupFile, writeFile, removeFile, renameFile, readFile, getUsedSpace } = require('./files');
const { zipFiles, unzipFiles } = require('./zip');

const USER_SPACE = parseInt(process.env.USER_SPACE, 10);//500Mo
const privateKey = require('crypto').randomFillSync(Buffer.alloc(20)).toString('base64');//Random private key

//Prepare your tables
const [ User, File ] = new Array(2).fill().map(() => ({}));
const tables = { User, File };
Object.assign(User, {
  login: 'string/25',
  password: 'binary/64',
  used: 'integer/4',
  total: 'integer/4',
  notNull: ['login', 'password', 'used', 'total'],
  index: ['login/unique']
});
Object.assign(File, {
  name: 'string/40',
  owner: User,
  createdAt: 'dateTime',
  lastModified: 'dateTime',
  notNull: ['name', 'owner', 'createdAt', 'lastModified'],
  index: [{
    column: ['name', 'owner'],
    type: 'unique',
  }]
});

//Log into your database solution like mysql
const database = {
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PWD,
  host: 'localhost',
  type: 'mysql',
  privateKey,
  database: 'cloudstorage',
  create: !!process.env.CREATE_DATABASE,
};

//Create your access table
const rules = {
  User: {
    login: {
      read: is('self')
    },
    password: {
      write: is('self'), //Only the password can be changed by the user
      read: none,        //No one can access the passwords
    },
    create : all,     //Creation is handled by login middleware. No one should create Users from request.
    delete : none,    //Users profile cannot be deleted from any request except from admin
    write : none,     //Users cannot edit their information except the password
    read : is('self'),//Users can only read their own storage data
  },
  File: {
    owner: {
      write: none,
    },
    create: is('owner'),
    delete: is('owner'),
    write: is('owner'),
    read: is('owner'),
  }
};
function toType(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
}

/** Ensure that the name is compatible with the file system. */
function isNameAllowed(name, isFolder) {
  if(!name || (name==='' && !isFolder) || name==='.' || name==='..') return false;
  if(name.includes('/')) return name.split('/').every(isNameAllowed, isFolder);
  return true;
}

const storagePlugin = {
  onRequest: {
    User: request => {
      //Set the default space value when creating a user
      if(request.create) {
        request.used = 0;
        request.total = USER_SPACE;
      }
    },
    File: async (request, { query, local: { authId }, isAdmin}) => {
      const isNameAString = Object(request.name) instanceof String;
      const folderName = isNameAString ? request.name.endsWith('/') ? request.name : request.name + '/' : request.name;
      const asFolder = isNameAString ? { like: folderName+'%'} : request.name;
      request.originalPath = request.name;
      request.folderName = folderName;
      //We interprete folders as a research for all files contained within
      if(isNameAString && request.name.endsWith('/')) request.name = asFolder;
      const now = Date.now();
      //Handle requesting a zip of resulting files, 
      if(request.zip) {
        //The content will already be included in the zip file
        if(request.get && request.get.includes('content')) request.get.splice(request.get.indexOf('content', 1));
        //If we are receiving content as zip, we need to unzip and create contained files in the database and the file system
        if(request.create || (request.set && request.set.content)) {
          if(!isNameAString) return Promise.reject({status: 400, message: `Name constraint must be a string in File requests when creating files from zip. We received ${typeof request.name} instead.`});
          const content = request.create ? request.content : request.set.content;
          if(!(Object(content) instanceof String)) return Promise.reject({status: 400, message: `The 'content' field must be the base64 encoded string of the file content, but we received a ${toType(content)}.`});
          const files = await unzipFiles(content);
          //Delete all the files from the folder if any
          await query({ File: { name: asFolder, delete: true }});
          //Create the new files
          await query({ File: files.map(({name, content}) => ({ name: folderName+name, content, create: true, createdAt: now, lastModified: now }))});
          //This part was just done manually. We remove it from the request
          delete request.create ? request.create : request.set.content;
        }
        //We will look for all files included into the folder
        request.name = asFolder;
      }
      //Set the creation time when creating a file
      if(request.create) {
        //Ensure that the name is allowed
        if(!isNameAString) return Promise.reject({status: 400, message: `Name constraint must be a string in File requests when creating files from zip. We received ${typeof request.name} instead.`});
        if(!isNameAllowed(request.originalPath, request.zip)) return Promise.reject({status: 400, message: `${request.originalPath} is not a valid pathname and cannot be created.`});
        //Set the creation time
        request.lastModified = now;
        request.createdAt = now;
      } else if(request.set) {
        request.set.lastModified = Date.now();
      }
      //We set the current user as the default owner for all File requests
      if(!(request.owner || isAdmin)) request.owner = { reservedId: authId };
    }
  },
  onUpdate: {
    File: async ({objects, newValues, oldValues}, { local }) => {
      //If the name changed, we need to rename the file on the filesystem too
      if(newValues.name) {
        //TODO handle changing folder name
        if(objects.length>1) return Promise.reject({status: 400, message: `You cannot rename multiple files with the same name, but you tried to edit the following files: ${Object.values(oldValues).map(({name}) => name).join(', ')} results were found.`});
        if(objects.length===0) return;
        const userId = objects[0].owner && objects[0].owner.reservedId;
        const oldName = oldValues[objects[0].reservedId].name;
        const newName = newValues.name;
        //Ensure that the name is allowed
        if(!isNameAllowed(newName, false)) return Promise.reject({status: 400, message: `${newName} is not a valid pathname and cannot be created.`});
        //If the file exist, we need to back it up
        try {
          await fileExist(userId, newName);
          await backupFile(userId, newName);
          if(!local.deleted) local.deleted = [];
          local.deleted.push({userId, filename: newName});
        } catch(err) {
          if(err.code!=='ENOENT') return Promise.reject(err);
        }
        //Rename the file on the file system
        await renameFile(userId, oldName, newName);
        //Mark the file as renamed for rolling back if needed
        if(!local.renamed) local.renamed = [];
        local.renamed.push({userId, oldName, newName});
      }
    }
  },
  onResult: {
    File: async (results, { request, query, local }) => {
      //If the content changed, we need to update the file content on the file system and update the user available space
      if(request.set && request.set.content) {
        const content = request.set.content;
        const editedUsers = []; // The users being concerned

        if(!(Object(content) instanceof String)) return Promise.reject({status: 400, message: `The 'content' field must be the base64 encoded string of the file content, but we received a ${toType(content)}.`});
        //Treating each File sequencially to make sure we can rollback properly
        await results.reduce(async (previousPromise, resolvedObject) => {
          await previousPromise;
          const userId = resolvedObject.owner && resolvedObject.owner.reservedId;
          const filename = resolvedObject.name;
          //We backup the file and mark it as updated in case we need to rollback
          await backupFile(userId, filename);
          if(!local.updated) local.updated = [];
          local.updated.push({filename, userId});
          //We erase the file
          await writeFile(userId, filename, Buffer.from(content, 'base64'));
          if(!editedUsers.includes(userId)) editedUsers.push(userId);
          return Promise.resolve();
        }, Promise.resolve());
        //We update the user's available space
        const queryContent = await Promise.all(editedUsers.map(async userId => ({
          reservedId: userId,
          get: ['total'],
          set: { used: await getUsedSpace(userId) }
        })));
        const { User: users } = await query({ User: queryContent }, { admin: true });
        const userOverflow = users.find(user => user.total < user.size);
        if(userOverflow) return Promise.reject({status: 507, message: `User ${userOverflow.reservedId} with ${Math.floor(userOverflow.size/1000)}ko exeeded the available space of ${userOverflow.total}.`});
      }
      //We read the file if the field "content" is requested
      if(request.get && request.get.includes('content')) {
        await Promise.all(results.map(async resolvedObject => {
          const userId = resolvedObject.owner && resolvedObject.owner.reservedId;
          const content = await readFile(userId, resolvedObject.name);
          resolvedObject.content = Buffer.from(content).toString('base64');
        }));
      }
      //We create a zip with the files contained in the result
      if(request.zip) {
        //Read the files content
        const filesContent = await Promise.all(results.map(({name, owner: { reservedId: userId }}) => readFile(userId, name)));
        let files = results.map(({name}) => name);

        //If we can, we remove the root path from the files paths
        if(Object(request.folderName) instanceof String) files = files.map(file => file.replace(request.folderName, ''));

        //Create the zip files
        const zip = await zipFiles(files.map((name, i) => ({ name, content: filesContent[i] })));
        
        //We empty the results to set only the zip as sole file.
        const name = request.originalPath;
        results.splice(0, results.length);
        results.push({ name, owner: request.owner, content: zip, files, zip: true });
      }
    }
  },
  onCreation: {
    //Create the storage folder for every new user
    User: async (createdObject, {query}) => {
      const userId = createdObject.reservedId;
      await createUserStorage(userId);
      //We set the available space for the user
      await query({ User: {
        reservedId: userId,
        set: { used: 0, total: USER_SPACE }
      }}, {admin: true});
    },
    File: async (createdObject, {query, request, local}) => {
      //When creating a file in the database, we need to create the file on the file system and update the user's available space
      const content = request.content;
      const userId = createdObject.owner.reservedId;
      const filename = createdObject.name;
      if(!(Object(content) instanceof String)) return Promise.reject({status: 400, message: `The 'content' field must be the base64 encoded string of the file content, but we received a ${typeof content}.`});
      const fileContent = Buffer.from(content, 'base64');
      await writeFile(userId, filename, fileContent);
      //Rememeber the file being created in case of rolling back
      if(!local.created) local.created = [];
      local.created.push({filename, userId});
      //We update the user's available space
      const size = await getUsedSpace(userId);
      const { User: [{total}] } = await query({
        //Set the space usage for the User
        User: { reservedId: userId, get: ['total'], set: { used: size }}
      }, { admin: true });
      if(total<size) return Promise.reject({status: 507, message: `User ${createdObject.owner.reservedId} with ${Math.floor(size/1000)}ko exeeded the available space of ${total}.`});
    }
  },
  onDeletion: {
    User: (deletedObjectsArray, {local}) => {
      //We will just mark the user as deleted. The content deletion will be made only on onSuccess
      if(!local.deletedUsers) local.deletedUsers = [];
      local.deletedUsers.push(...deletedObjectsArray.map(u => u.reservedId));
    },
    File: async (deletedObjectsArray, {query, local}) => {
      const editedUsers = []; // The users being concerned
      await Promise.all(deletedObjectsArray.map(async file => {
        const userId = file.owner && file.owner.reservedId;
        const filename = file.name;
        await backupFile(userId, filename);
        if(!local.deleted) local.deleted = [];
        local.deleted.push({userId, filename});
        if(!editedUsers.includes(userId)) editedUsers.push(userId);
      }));
      //Update available space per user
      const queryContent = await Promise.all(editedUsers.map(async userId => ({
        reservedId: userId,
        set: { used: await getUsedSpace(userId) }
      })));
      return query({ User: queryContent }, { admin: true });
    }
  },
  onError: async (results, { local: { created = [], deleted = [], updated = [], renamed = [] } }) => {
    //We need to delete the created files, put back the edited files and rename the backup files
    await Promise.all(renamed.map(({userId, oldName, newName}) => renameFile(userId, newName, oldName)));
    await Promise.all(created.map(({userId, filename}) => removeFile(userId, filename)));
    await Promise.all(updated.map(({userId, filename}) => restoreBackup(userId, filename)));
    await Promise.all(deleted.map(({userId, filename}) => restoreBackup(userId, filename)));
  },
  onSuccess: async (results, { local: { deleted = [], updated = [], deletedUsers = [] }}) => {
    //We can delete the backups
    await Promise.all(deleted.map(({userId, filename}) => removeBackup(userId, filename)));
    await Promise.all(updated.map(({userId, filename}) => removeBackup(userId, filename)));
    await Promise.all(deletedUsers.map(destroyUserStorage));
  }
};

//Create the app
const app = express();

//Create your plugins
const plugins = [
  loginPlugin({
    login: 'login',
    password: 'password',
    userTable: 'User'
  }),
  storagePlugin,
];
app.listen(process.env.PORT);

// Create the server (we export for testing purpose only)
module.exports = createServer({app, tables, database, rules, plugins}, { sizeLimit: '50mb' }).catch(err => console.error(err) || process.exit(1));