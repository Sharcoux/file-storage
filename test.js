const crypto = require('crypto');
const Testing = require('simple-ql-testing');
const fs = require('fs');

process.env.PORT = 80;
const log = require('./logger');

//We will use this as the password for our fake users.
const userHashedPassword = crypto.pbkdf2Sync('password', '', 1, 64, 'sha512').toString('base64');

fs.rmdirSync('./storage', {recursive: true});

const test = Testing('/');
const createTest = Testing.createTest;

require('./')//Create test server
  .then(() => log('info', '\n', 'Test server ready'))

  .then(() => test([
    createTest(true, 'Registration of 2 users',
      {
        User : [
          {
            pseudo : 'User1',
            login : 'user1@email.com',
            password: userHashedPassword,
            create : true,
          },
          {
            pseudo : 'User2',
            login : 'user2@email.com',
            password: userHashedPassword,
            create : true,
          }
        ]
      }),

    createTest(true, 'Login as User1',
      {
        User: {
          login: 'user1@email.com',
          password: userHashedPassword,
        }
      })
  ]))
  
  //Use the jwt for user1 
  .then(response => Testing.setJWT(response.data.User[0].jwt))

  .then(() => test([
    createTest(false, 'Forbidden: Create file with no name',
      {
        File: {
          content: 'test',
          create: true
        }
      }),

    createTest(true, 'Create file with user 1',
      {
        File: {
          name: 'test',
          content: 'test',
          create: true
        }
      }),

    createTest(false, 'Forbidden: Create a file with same name',
      {
        File: {
          name: 'test',
          content: 'test',
          create: true
        }
      }),

    createTest(false, 'Forbidden: Create a file with name ending with /',
      {
        File: {
          name: 'test3/',
          content: 'test',
          create: true
        }
      }),

    createTest(true, 'Get a file content',
      {
        File: {
          name: 'test',
          get: ['content']
        }
      }),

    createTest(true, 'Update a file content',
      {
        File: {
          name: 'test',
          set: {
            content: 'test2'
          }
        }
      }),

    createTest(true, 'Ensure file content changed',
      {
        File: {
          name: 'test',
          get: ['content']
        }
      }, { File: [{ reservedId: 1, name: 'test', owner: {reservedId: 1}, content: 'test'}]}),

    createTest(true, 'Create file with subfolders',
      {
        File: {
          name: 'folder1/folder2/test',
          content: 'test',
          create: true
        }
      }),

    createTest(true, 'Delete file with subfolders',
      {
        File: {
          name: 'folder1/folder2/test',
          delete: true
        }
      })
  ]))
  
  .then(() => fs.existsSync('./storage/folder1/folder2') && Promise.reject('Empty folder still exists'))

  .then(() => test([
    createTest(true, 'Create a List of files in a folder',
      {
        File: [
          {
            name: 'folder1/folder2/test',
            content: 'test',
            create: true
          },
          {
            name: 'folder1/folder2/test2',
            content: 'test2',
            create: true
          },
        ]
      }),

    createTest(true, 'Read a folder as a zip',
      {
        File: {
          name: 'folder1',
          zip: true,
        }
      }),

    createTest(true, 'Send a folder as a zip',
      {
        File: {
          name: 'folder3',
          content: 'UEsDBAoAAAAAANpzMVAAAAAAAAAAAAAAAAAIAAAAZm9sZGVyMi9QSwMECgAAAAAA2nMxULuTgcQDAAAAAwAAAAwAAABmb2xkZXIyL3Rlc3S16y1QSwMECgAAAAAA2nMxULuTgcQDAAAAAwAAAA0AAABmb2xkZXIyL3Rlc3QytestUEsBAhQACgAAAAAA2nMxUAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAQAAAAAAAAAGZvbGRlcjIvUEsBAhQACgAAAAAA2nMxULuTgcQDAAAAAwAAAAwAAAAAAAAAAAAAAAAAJgAAAGZvbGRlcjIvdGVzdFBLAQIUAAoAAAAAANpzMVC7k4HEAwAAAAMAAAANAAAAAAAAAAAAAAAAAFMAAABmb2xkZXIyL3Rlc3QyUEsFBgAAAAADAAMAqwAAAIEAAAAAAA==',
          zip: true,
          create: true,
        }
      }),

    createTest(true, 'Read all files in a folder',
      {
        File: {
          name: 'folder3/',
          get: ['content']
        }
      }),

    createTest(true, 'Rename a file',
      {
        File: {
          name: 'folder1/folder2/test2',
          set: {
            name: 'folder1/folder2/test3'
          }
        }
      }),

    createTest(false, 'Forbidden: Rename a file to an existing location',
      {
        File: {
          name: 'folder1/folder2/test3',
          set: {
            name: 'folder1/folder2/test'
          }
        }
      })
  ]))

  .catch(err => console.error(err)).then(() => process.exit());
